# **DEPRECATED**

# Latest updates at: https://github.com/9klips/CS-1.6-CS-CZ-Configs

### CS-1.6-CS-CZ-Configs

 INSTALL:
 Put the file userconfig.cfg in ...\Steam\steamapps\common\Half-Life\cstrike for CS 1.6 and in ...\Steam\steamapps\common\Half-Life\czero

Make both userconfig.cfg and cs.cfg read-only from properties.